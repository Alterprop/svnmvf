import xmlParser from './xml2json';
import http from './http';

const getFilePath = path =>
  path
    .split('/')
    .slice(0, path.split('/').length - 1)
    .join('/');
const getFileName = path =>
  path
    .split('/')
    .slice(path.split('/').length - 1, path.split('/').length)
    .join('/');

const createPathObj = (log, path) => ({
  codigo: log.msg.split('|')[0],
  descripcion: log.msg.split('|')[2],
  entrega: log.msg.split('|')[1],
  sistema: 'Mi VF',
  ruta: getFilePath(path),
  objeto: getFileName(path),
  status: log.status,
  numCr: '',
  personal: log.author,
  comentarios: log.msg.split('|')[3],
  sit: '',
  sit2: '',
  ppdr1: '',
  oculto: '',
  produccion: '',
  crq: '',
  revision: log.revision,
  date: log.date
});

const createRow = (log) => {
  return `${Object.keys(log).map(field => log[field])}\n`;
};

const createTableRow = (log, path) => {
  return createPathObj(log, path);
};

const parseLog = (logData, options) => xmlParser(logData).log.logentry;

/*
.logentry.filter(
      log => options.revision ? log.revision > options.revision : true
    )
*/

const getXML = logNodes => {
  const parserContainer = new DOMParser();
  return parserContainer.parseFromString(logNodes, 'text/xml');
};

const logPrint = (logData, options) =>
  logData
    .map(
      log =>
        Array.isArray(log)
          ? log
            .map(path => createRow(path) ).join('')
          : createRow(log)
    )
    .join('');

const logTable = (logData, options) =>
  parseLog(logData, options).map(log => {
    return Array.isArray(log.paths.path)
      ? log.paths.path.map(path => {
          return createTableRow(
            Object.assign({}, log, {
              msg: log.paths.msg,
              status: log.paths.path.action || 'M'
            }),
            path
          );
        })
      : createTableRow(Object.assign({}, log, { status: 'M' }), log.paths.path);
  });

const sendLogData = (logData, options) => {
  if (!logData) {
    return console.log('Ningún dato desde SVN');
  }
  return options.print
    ? logPrint(logData, options)
    : logTable(logData, options);
};

const getFromFileSystem = options => {
  return new Promise((resolve, reject) => {
    options.file
      ? resolve(getXML(options.file))
      : reject({ error: 'No hay datos' });
  })
    .then(logData => sendLogData(logData, options))
    .catch(error => error);
};

const getFromLogsFolder = options => {
  return http
    .get(`logs/${options.file}`)
    .then(response => response.text())
    .then(getXML)
    .then(logData => sendLogData(logData, options))
    .catch(error => error);
};

export default function(options) {
  return options && options.fs
    ? getFromFileSystem(options)
    : logPrint(options.file, options);
}
