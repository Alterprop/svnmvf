import PouchDB from 'pouchdb';
import {ObjectID} from 'bson';

const localDB = new PouchDB('svnxmldb');
const remoteDB = new PouchDB('http://localhost:5984/svnxmldb');

const insert = (doc) => {
  let _id = new ObjectID();

  return localDB.put(
    Object.assign({}, doc, {
      _id: _id.toString(),
    })
  );
};

const update = (updt) => {
  if (Array.isArray(updt._id)) return localDB.bulkDocs(updt);

  return localDB.get(
    updt._id
  ).then(
    (doc) => localDB.put(
      Object.assign(
        {},
        doc,
        updt
      )
    )
  );
};

const findOne = (id) => localDB.get(id);

const find = (ext) => {
  let opts = {
    include_docs: true,
  };

  return localDB.allDocs(Object.assign({}, opts, {}));
};

const remove = (_id) =>
  localDB
    .get(_id)
    .then((doc) => localDB.remove(doc))
    .catch((err) => console.log(err));

const sync = (opciones) => {
  if (!opciones) {
    opciones = {
      live: true,
      retry: false,
    };
  }
  return localDB.sync(remoteDB, opciones);
};

const syncEvs = () =>
  sync()
    .on('change', (change) => change)
    .on('paused', (info) => info)
    .on('active', (info) => info)
    .on('complete', (info) => info)
    .on('error', (err) => err);

const unsync = () => sync.cancel();

const init = () => localDB.info(); //sync())

export default {
  init: init,
  insert: insert,
  update: update,
  find: find,
  findOne: findOne,
  remove: remove,
  sync: sync,
  syncEvs: syncEvs,
  unsync: unsync
};
