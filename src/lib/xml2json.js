// http://davidwalsh.name/convert-xml-json
export default function xmlToJson(xml) {
  let obj = {};

  if (xml.nodeType === 1) {
    if (xml.attributes.length) {
      [].slice.call(xml.attributes)
        .forEach(
          (attr) => obj[attr.nodeName] = attr.nodeValue
        );
    }
  } else if (xml.nodeType === 3) {
    obj = xml.nodeValue;
  }

  if (xml.hasChildNodes()) {
    xml.childNodes.length === 1 && xml.childNodes[0].nodeType === 3
      ? (obj = xml.childNodes[0].nodeValue)
      : [].slice.call(xml.childNodes).forEach((node) => {
        const nodeName = node.nodeName;
        if (typeof obj[nodeName] === 'undefined') {
          obj[nodeName] = xmlToJson(node);
        } else {
          if (!Array.isArray(obj[nodeName])) {
            obj[nodeName] = [].concat( [obj[nodeName]] );
          }
          obj[nodeName] = [].concat(obj[nodeName], xmlToJson(node));
        }
      });
  }

  return obj;
}
