const proto = {
  get: url => fetch(url)
};

export default Object.create(proto);
