import React from 'react';
import {Button} from '../ui';
import DB from '../../lib/db';

const buttonFunc = (_id) => DB.remove(_id)
  .then((res) => alert('El registro ha sido borrado de la BBDD'))
  .catch((err) => alert('KO') )

export default function DeleteButton(props) {
  return (
    <Button
      buttonFunc={() => buttonFunc(props.itemId) }
      text="Borra" />
  );
}
