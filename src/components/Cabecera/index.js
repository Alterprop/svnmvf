import React from 'react';
import PropTypes from 'prop-types';
import {
  Header
} from '../ui';

function Cabecera(props) {
  return (
    <Header
      {...props}
      role
    />
  );
}

Cabecera.defaultProps  = {
  title: null,
}

Cabecera.propTypes = {
  title: PropTypes.string,
};

export default Cabecera;
