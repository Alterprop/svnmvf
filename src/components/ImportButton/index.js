import React from 'react';
import UploadButton from '../UploadButton';
import DB from '../../lib/db';
import getLog from '../../lib/svn';

const importFn = (event) => {
  const reader = new FileReader();
  reader.onloadend = (ev) => getLog(
    {
      file: ev.target.result,
      fs: true,
    }
  ).then(
    (response) => DB.insert({log: response})
  ).then(
    (response) => console.log(response)
  ).catch(
    (error) => console.log(error)
  );
  reader.readAsText(event.target.files[0], 'utf-8');
};

export default function ImportButton(props) {
  return (
    <UploadButton uploadFn={importFn} />
  );
}
