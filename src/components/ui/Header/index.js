import React from 'react';
import PropTypes from 'prop-types';
import Nav from '../Nav';
import './Header.css';

function Header(props) {
  return (
    <header
      role={props.role ? 'banner' : null}
      aria-label={props.role ? 'Main Header' : 'Section Header'}
    >
      {props.leftItems ? <Nav items={props.leftItems} /> : null}
      {props.rightItems ? <Nav items={props.rightItems} /> : null}
      <h1>{props.title}</h1>
    </header>
  );
}

Header.defaultProps = {
  title: 'svnmvf',
};

Header.propTypes = {
  title: PropTypes.string,
};

export default Header;
