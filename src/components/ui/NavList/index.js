import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import Button from '../Button';

export default function NavList(props) {
  return (
    <ul>
      {props.items.map((item, idx) => {
        const index = idx * Date.now();
        return item.link ? (
          <li key={index}>
            <Link href={item.link} to={item.link}>
              {item.text}
            </Link>
          </li>
        ) : (
          <li key={index}>
            <Button
              buttonFunc={item.action}
              icon={item.icon}
              size="16"
              type="lineal"
              text={item.text}
            />
          </li>
        );
      })}
    </ul>
  );
}

NavList.defaultProps = {
};

NavList.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({
    action: PropTypes.func,
    icon: PropTypes.string,
    text: PropTypes.string,
    link: PropTypes.string,
    active: PropTypes.string,
  })).isRequired,
};
