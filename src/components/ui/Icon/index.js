import React from 'react';
import PropTypes from 'prop-types';
import iconos from './iconos';

export default function Icon(props) {
  return (
    <svg
      width={props.dimension || '24'}
      height={props.dimension || '24'}
      viewBox="0 0 24 24"
      className={props.clase}
    >
      <path
        fill={props.fill || 'rgba(0,0,0,0.56)'}
        stroke={props.stroke}
        strokeWidth={props.strokeWidth}
        d={iconos(props.forma, (props.plataforma || 'web'))}
      />
    </svg>
  );
}

Icon.defaultProps = {
  fill: 'rgba(0,0,0,0.56)',
  stroke: null,
  strokeWidth: null,
  plataforma: 'web',
  clase: null,
  dimension: '24',
};

Icon.propTypes = {
  fill: PropTypes.string,
  stroke: PropTypes.string,
  strokeWidth: PropTypes.number,
  shape: PropTypes.string.isRequired,
  plataform: PropTypes.oneOf(['web', 'android']),
  size: PropTypes.string,
};
