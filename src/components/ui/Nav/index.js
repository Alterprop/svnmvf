import React from 'react';
import PropTypes from 'prop-types';
import NavList from '../NavList';
import './Nav.css';

export default function Nav(props) {
  return (
    <nav
      role={props.role ? 'navigation' : null}
      className={props.role ? 'nav' : 'nav'}
    >
      <NavList items={props.items} />
    </nav>
  );
}

Nav.defaultProps = {
  role: null,
};

Nav.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({
    action: PropTypes.func,
    icon: PropTypes.string,
    text: PropTypes.string,
    link: PropTypes.string,
  })).isRequired,
  role: PropTypes.string,
};
