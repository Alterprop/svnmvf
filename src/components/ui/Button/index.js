import React from 'react';
import PropTypes from 'prop-types';
import './Button.css';
import Icon from '../Icon';

export default function Button(props) {
  return (
    <button
      onClick={props.buttonFunc}
      className={
        props.icon
          ? `btn__iconico btn__iconico--${props.type}`
          : `btn__button btn__button--${props.type}`}
    >
      {props.buttonIcon ?
        <Icon
          shape={props.icon}
          size={props.iconSize}
        /> :
        null
      }
      {props.text ? <span>{props.text}</span> : null}
    </button>
  );
}

Button.defaultProps = {
  icon: null,
  iconSize: null,
  type: 'normal',
  text: null,
};

Button.propTypes = {
  buttonFunc: PropTypes.func.isRequired,
  type: PropTypes.string,
  iconSize: PropTypes.string,
  text: PropTypes.string,
};
