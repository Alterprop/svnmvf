import React from 'react';
import DB from '../../lib/db';
import getLog from '../../lib/svn';
import {Button} from '../ui';

const exportFn = (_id) => {
  DB.findOne(_id)
    .then((response) => getLog({ file: response.log, print: true }) )
    .then((response) => window.open(`data:text/csv;charset=UTF-8,${response}`))
    .catch(
      (error) => console.log(error)
    );
};

export default function ImportButton(props) {
  return (
    <Button buttonFunc={() => exportFn(props.itemId)}
      text="Exporta"
      type="primary"
    />
  );
}
