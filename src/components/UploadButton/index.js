import React from 'react';
import { Button } from '../ui';
import './UploadButton.css';

const buttonFunc = event => event.target.nextSibling.click();

export default function UploadButton(props) {
  return (
    <div className="upload-button">
      <Button
        buttonFunc={buttonFunc}
        type="primary"
        text={props.text || 'Importa un repo'} />
      <input
        type="file"
        className=""
        name="upload"
        autoComplete="off"
        accept=".xml"
        onChange={props.uploadFn}
      />
    </div>
  );
}
