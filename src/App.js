import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import * as Views from './views';
import './App.css';

const viewsMap = [
  {
    name: 'home',
    isExact: true,
  },
  {
    name: 'table',
    isExact: false,
  },
];

const isExact = (view) =>
  viewsMap.find((v) => v.name === view.toLowerCase() ).isExact;

const getViewPath = (view) =>
  isExact(view) ? view.toLowerCase() : `${view.toLowerCase()}/:id`;

const routes = Object.keys(Views).map(
  (view, index) => <Route
    key={`${index}-${view.toLowerCase}`}
    exact={isExact(view)}
    path={ view === 'Home' ? '/' : `/${getViewPath(view)}` }
    component={Views[view]} />
);

export default function() {
  return (
    <Router>
      <Switch>
        {routes}
      </Switch>
    </Router>
  );
}
