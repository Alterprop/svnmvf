import React, { Component } from 'react';
import RepoItem from '../RepoItem';
import DB from '../../lib/db';
import getLog from '../../lib/svn';
import { UploadButton } from '../../components';
import './RepoList.css';

const getRepoItem = (item, index) => (
  <RepoItem key={index * Date.now()} item={item} />
);
const repoItem = (item, index) =>
  Array.isArray(item) ? item.map(getRepoItem) : getRepoItem(item, index);

class RepoList extends Component {
  constructor(props) {
    super(props);
    this.updateLogs = this.updateLogs.bind(this);
    this.importFn = this.importFn.bind(this);
    this.state = {
      items: []
    };
  }

  componentDidMount() {
    this.updateLogs();
  }

  updateLogs() {
    DB.find().then(
      res =>
        this.setState({ items: res.rows }, () =>
          console.log('STATE', this.state)
        ),
      err => console.log(err)
    );
  }

  importFn(event) {
    console.log(event);
    event.preventDefault();
    const reader = new FileReader();
    reader.onloadend = (ev) =>
      getLog({
        file: ev.target.result,
        fs: true
      })
        .then(response => DB.insert({ log: response }))
        .then(response => this.updateLogs())
        .catch(error => console.log(error));
    reader.readAsText(event.target.files[0], 'utf-8');
  }

  render() {
    return (
      <main className="repo-list">
        <h3>{ this.state.items.length ? 'Logs' : 'Importa el primer log' }</h3>
        <ul>{this.state.items.map(repoItem)}</ul>
        <UploadButton
          uploadFn={this.importFn}
          text={this.state.items.length ? 'Nuevo repo' : 'Importa un repo'} />
      </main>
    );
  }
}

export default RepoList;
