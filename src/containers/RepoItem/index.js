import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

class RepoItem extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <li>
        <Link to={`table/${this.props.item.id}`}>
          <h3>{this.props.item.doc.log[0].codigo}</h3>
          <span>ID : {this.props.item.id}</span>
        </Link>
      </li>
    );
  }
}

RepoItem.defaultProps = {};

RepoItem.propTypes = {
  item: PropTypes.shape({
    revision: PropTypes.string,
    author: PropTypes.string,
    date: PropTypes.string,
  }).isRequired,
};

export default RepoItem;
