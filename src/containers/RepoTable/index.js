import React, { Component } from 'react';
import PropTypes from 'prop-types';
import DB from '../../lib/db';
import './RepoTable.css';

const tableTitles = [
  'Código',
  'Descripción',
  'Entrega',
  'Sistema',
  'Ruta',
  'Objeto',
  'Status',
  'numCr',
  'Personal',
  'Comentarios',
  'Sit',
  'Sit2',
  'Pprd1',
  'Oculto',
  'Producción',
  'CRQ',
  'Revisión',
  'Fecha'
];

const selectOptions = [
  { label: 'Pendiente' },
  { label: 'Desplegado' }
];

const selectOnChange = (itemValue, index, items) => {

  console.log(Object.keys(items)
    .map((item, idx) => idx === index ? itemValue : items[item] ));
};

const tdValue = (item, index, items) => <td key={index * Date.now()}>
  {
    index > 9 && index < 16
      ? <select onChange={(event) => selectOnChange(event.target.value, index, items)}>{
        selectOptions.map(
          (option, idx) => <option key={idx} value={option.label.toLowerCase()}>
            {option.label}
          </option>
        )
      }
      </select>
      :   item
  }
</td>;

const getTd = item =>
  Object.keys(item).map((td, index) => tdValue(item[td], index, item));

class RepoTable extends Component {
  constructor(props) {
    super(props);

    this.state = {
      items: []
    };
  }

  componentDidMount() {
    DB.findOne(this.props.itemId).then(
      res =>
        this.setState({ items: res.log }, () =>
          console.log('STATE', this.state)
        ),
      err => console.log(err)
    );
  }

  render() {
    return (
      <table cellSpacing="0" cellPadding="0">
        <thead>
          <tr>{tableTitles.map((key, index) => <th key={index}>{key}</th>)}</tr>
        </thead>
        <tbody>
          {this.state.items.map(
            (item, index) =>
              Array.isArray(item) ? (
                item.map((it, idx) => (
                  <tr key={idx * Date.now()}>{getTd(it)}</tr>
                ))
              ) : (
                <tr key={`index-${index * Date.now()}`}>{getTd(item)}</tr>
              )
          )}
        </tbody>
      </table>
    );
  }
}

RepoTable.defaultProps = {};

RepoTable.propTypes = {
  itemId: PropTypes.string.isRequired
};

export default RepoTable;
