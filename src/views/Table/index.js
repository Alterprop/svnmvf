import React from 'react';
import { RepoTable } from '../../containers';
import './Table.css';

import  {
  Cabecera,
  DeleteButton,
  ExportButton,
} from '../../components';

const leftItems = [
  {
    text: 'back',
    link: '/',
  },
];

const rightItems = [];

const Table = (props) => (
  <div className="page">
    <Cabecera
      title="SVNMVF"
      leftItems={leftItems}
      rightItems={rightItems} />
    <main className="repo-table__content">
      <RepoTable itemId={props.match.params.id} />
    </main>
    <footer className="repo-table__footer">
      <DeleteButton itemId={props.match.params.id} />
      <ExportButton itemId={props.match.params.id} />
    </footer>
  </div>
);

export default Table;
