import React from 'react';

import {
  Cabecera,
  ImportButton,
} from '../../components';

import {
  RepoList,
} from '../../containers';

const Home = (props) => (
  <div className="page">
    <Cabecera
      title="SVNMVF" />
    <RepoList />
  </div>
);

export default Home;
